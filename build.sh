#!/usr/bin/env bash

set -xe

DOCKER_CMD="docker run --rm -it -v $(pwd):/documents/ asciidoctor/docker-asciidoctor asciidoctor-revealjs -r asciidoctor-diagram"

$DOCKER_CMD -a revealjsdir=reveal.js presentation.adoc
